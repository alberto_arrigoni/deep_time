import tensorflow as tf
from dp_time.utils import set_seed_and_reset_graph
import logging
import numpy as np
import pandas as pd

logger = logging.getLogger('deep_time')


class RecurrentNetwork:
    def __init__(self, n_steps=20, n_inputs=1, n_neurons=100, n_outputs=1,
                 seed=42, learning_rate=0.001, lstm_cell=True, n_layers=1,
                 keep_probability=1):
        """
        :param n_steps: number of RNN/LSTM cells
        :param n_inputs: dimensionality of input vector (single step)
        :param n_neurons: number of neurons per step (single RNN/LSTM step)
        :param n_outputs: output size of the projection operation ()
        :param seed:
        :param learning_rate:
        :param lstm_cell: (boolean), default is True
        :param n_layers: number of layers (RNN/LSTM)
        :param keep_probability: keep probability (dropout filter)
        """
        if seed:
            set_seed_and_reset_graph(seed)
        self.n_steps = n_steps
        self.n_inputs = n_inputs
        self.n_neurons = n_neurons
        self.n_outputs = n_outputs
        self.learning_rate = learning_rate
        self.lstm_cell = lstm_cell
        self.n_layers = n_layers
        self.n_neuron = n_neurons
        self.keep_probability = keep_probability
        self._build_graph()
        self.saver = None
        self.model_output_path = None

    def _build_graph(self):
        self.X = tf.placeholder(tf.float32, [None, self.n_steps, self.n_inputs])
        self.y = tf.placeholder(tf.float32, [None, self.n_steps, self.n_outputs])

        if self.n_layers == 1:
            cell = tf.contrib.rnn.BasicLSTMCell(num_units=self.n_neurons, activation=tf.nn.relu)
            self.rnn_outputs, self.states = tf.nn.dynamic_rnn(cell, self.X, dtype=tf.float32)
        elif self.n_layers > 1:
            layers = [tf.contrib.rnn.DropoutWrapper(tf.contrib.rnn.BasicLSTMCell(num_units=self.n_neurons),
                                                    input_keep_prob=self.keep_probability)
                      for layer in range(self.n_layers)]
            multi_layer_cell = tf.contrib.rnn.MultiRNNCell(layers)
            self.rnn_outputs, self.states = tf.nn.dynamic_rnn(multi_layer_cell, self.X, dtype=tf.float32)
        else:
            raise ValueError('"n_layers" must be an integer >= 1')

        self.outputs = self._reshape_rnn_output()
        self.loss = tf.reduce_mean(tf.square(self.outputs - self.y))
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        self.training_op = self.optimizer.minimize(self.loss)
        self.saver = None
        self.model_output_path = None

    def _reshape_rnn_output(self):
        stacked_rnn_outputs = tf.reshape(self.rnn_outputs, [-1, self.n_neurons])
        stacked_outputs = tf.layers.dense(stacked_rnn_outputs, self.n_outputs)
        outputs = tf.reshape(stacked_outputs, [-1, self.n_steps, self.n_outputs])
        return outputs

    def fit(self, dataset, batch_size=50, n_iterations=1500,
            model_output_path="data/recurrent_model",
            log_path='data/logs'):
        """
        Fit RNN
        :param dataset: must implement the Dataset interface with data property != False, e.g. RecurrentTs
        :param batch_size: number of time series (a single ts is identified by the 'grouping_variable'
        of the Dataset class)
        :param n_iterations:
        :param model_output_path: (str) path where the Tensorflow model is saved
        :param log_path: (str) log output path (Tensorflow)
        """
        self.model_output_path = model_output_path
        init = tf.global_variables_initializer()
        self.saver = tf.train.Saver()

        with tf.Session() as sess:
            init.run()
            for iteration in range(n_iterations):
                X_batch, y_batch = dataset.next_batch(batch_size, self.n_steps)
                sess.run(self.training_op, feed_dict={self.X: X_batch, self.y: y_batch})
                if iteration % 100 == 0:
                    mse = self.loss.eval(feed_dict={self.X: X_batch, self.y: y_batch})
                    logger.debug("iteration\tMSE: {}".format(mse))

            self.saver.save(sess, model_output_path)
        tf.summary.FileWriter(log_path, tf.get_default_graph()).close()
        logger.debug('Go to {} to start Tensorboard using "tensorboard --logdir=..."'.format(log_path))

    def predict(self, X_new):
        if not self.model_output_path:
            raise ValueError('You need to train and save a model first')

        with tf.Session() as sess:
            self.saver.restore(sess, self.model_output_path)
            return sess.run(self.outputs, feed_dict={self.X: X_new})

    def generate_future_obs(self, seed, n_steps=3):
        with tf.Session() as sess:
            self.saver.restore(sess, self.model_output_path)
            for iteration in range(n_steps):
                X_batch = np.array(seed[-self.n_steps:]).reshape(1, self.n_steps, 1)
                y_pred = sess.run(self.outputs, feed_dict={self.X: X_batch})
                seed.append(y_pred[0, -1, 0])
            return seed

    @staticmethod
    def _format_future_prediction_df(feature_space, predictive_feature_space):
        """
        'feature_space' and 'predictive_feature_space' refer to one time series (see grouping variable 'category')
        :param feature_space:
        :param predictive_feature_space:
        :return: a list of dataframes to be used as seeds for prediction from a trained model
        """
        X_batch_list = []
        feature_space = feature_space.drop(columns='target', axis=1)
        if 'target' in predictive_feature_space.columns:
            predictive_feature_space = predictive_feature_space.drop('target', 1)
        for iteration in range(1, predictive_feature_space.shape[0]+1):
            X_batch = pd.concat([feature_space.tail(feature_space.shape[0]-iteration),
                                 predictive_feature_space.head(iteration)]).reset_index(drop=True)
            if 'category' in X_batch.columns:
                X_batch = X_batch.drop('category', 1)
            X_batch_list.append(X_batch)

        return X_batch_list

    def generate_future_obs_from_df(self, feature_space, predictive_feature_space, feed_prediction_column=False):
        """
        Use the trained RNN as a generative model
        :param feature_df: (a pandas df)
        :param predictive_feature_space: (a pandas df)
        :param feed_prediction_column: column to be replaced by rolling prediction
        :return:
        """
        lagged_target_list = None

        results_list = []
        with tf.Session() as sess:
            self.saver.restore(sess, self.model_output_path)
            for X_batch in RecurrentNetwork._format_future_prediction_df(feature_space, predictive_feature_space):
                if feed_prediction_column and lagged_target_list:
                    X_batch.loc[:, feed_prediction_column].iloc[-len(lagged_target_list):] = lagged_target_list

                reshaped_X_batch = X_batch.as_matrix().reshape(1, self.n_steps, self.n_inputs)
                results_list.append(sess.run(self.outputs, feed_dict={self.X: reshaped_X_batch}))

                lagged_target_list = [i[-1][-1][0] for i in results_list]

        return results_list


