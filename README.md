# deep_time

Deep learning for time series prediction


Create a simple (univariate time series model):
```python
%load_ext autoreload
%autoreload 2
from dp_time.models import RecurrentNetwork
from dp_time.dataset.rnn_dataset import MockTimeSeries

mock_ts = MockTimeSeries()

model = RecurrentNetwork()
model.fit(mock_ts)
```

Visualize the results on test set:
```python
X_new = mock_ts.create_mock_ts(n_inputs=1, n_steps=21)
X_train = X_new[:, :-1, :]
x_test = X_new[0, -1, 0]
y_pred = model.predict(x_training)


predictions = [None]
predictions.extend(y_pred[0, :, 0])
pd.DataFrame({'Actual': X_new[0, :, 0], 
              'Forecast': predictions}).plot(marker='o')
plt.title("Testing the model", fontsize=14)
plt.legend(loc="upper left")
plt.xlabel("Time")
plt.show()
```
![Image of RNN prediction](https://image.ibb.co/hu16AJ/prediction.png)



Use RNN as generative models:
```python
seqs = model.generate_future_obs(n_steps=450)
pd.Series(seqs).plot(marker='o')
```

![Image of RNN as gen model](https://image.ibb.co/bsRCjd/generative.png)

## Support for multivariate time series

```python
from dp_time.models import RecurrentNetwork
from dp_time.dataset.rnn_dataset import RecurrentTs
import numpy as np

recurrent_ts = RecurrentTs(pandas_df, 
                           target_only=False, 
                           one_hot_root_list=['product_category', 'segment'])

model = RecurrentNetwork(n_steps=8, n_inputs=32, n_neurons=100, 
                         n_layers=3, keep_probability=0.6)
model.fit(recurrent_ts, n_iterations=3000, batch_size=50, 
          model_output_path='data/model_for_prediction')
```

### Then test prediction on training set:
```python
import pandas as pd 
%matplotlib inline # For Jupyter notebooks

x_training, y_test = recurrent_ts.next_batch(1, 8, verbose=False)
y_pred = model.predict(x_training)

actual_vec = y_test.reshape(-1, 1)
pred_vec = y_pred.reshape(-1, 1)

# With Pandas plot
pd.DataFrame({'actual': [i[0] for i in actual_vec],
              'prediction': [max(0, int(i)) for i in pred_vec]}).plot(marker='o')
```