import unittest
import os
import pandas as pd
from dp_time.models import RecurrentNetwork

TEST_DIR = os.path.dirname(os.path.abspath(__file__))


class TestRecurrentNetwork(unittest.TestCase):
    def setUp(self):
        self.feature_space = pd.DataFrame({'feature_1': [i for i in range(10)],
                                           'feature_2': [i for i in range(10)],
                                           'target': [i for i in range(10)]})

        self.predictive_feature_space_single = pd.DataFrame({'feature_1': [42000],
                                                             'feature_2': [42000],
                                                             'target': [42000]}, index=[0])

        self.predictive_feature_space_multiple = pd.DataFrame({'feature_1': [i for i in range(100)],
                                                      'feature_2': [i for i in range(100)],
                                                      'target': [i for i in range(100)]})

    def test_generate_future_from_df(self):
        """
        Test use of a trained RNN model as generative model
        :return:
        """

        results = RecurrentNetwork._format_future_prediction_df(self.feature_space,
                                                                self.predictive_feature_space_single)
        self.assertEqual(len(results), 1)


if __name__ == '__main__':
    unittest.main()