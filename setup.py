from setuptools import setup, find_packages

setup(name='dp_time',
      version='0.0.1',
      description='Time series analysis',
      author='Alberto Arrigoni',
      author_email='arrigonialberto86@gmail.com',
      url='Bitbucket...',
      requires=['tensorflow', 'numpy', 'pandas'],
      packages=find_packages()
     )